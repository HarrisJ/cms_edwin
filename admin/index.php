<?php include "includes/admin_header.php"; ?>
    
        <div id="wrapper">

        <!-- Navigation -->
        <?php include "includes/admin_navigation.php"; ?>


        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Welcome Admin
                            <small><?php echo $_SESSION['username']?></small>
                        </h1>
                    </div>
                </div>
								<!-- /.row -->
							
								<div class="row">
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-file-text fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">

										<?php 
										
										$query = "SELECT * FROM posts";
										$result = mysqli_query($connect,$query);
										$post_count = mysqli_num_rows($result);

										echo "<div class='huge'>$post_count</div>";
										
										
										?>
										
                  
                        <div>Posts</div>
                    </div>
                </div>
            </div>
            <a href="post.php">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-green">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-comments fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">

										<?php 
										
										$query = "SELECT * FROM comments";
										$result = mysqli_query($connect,$query);
										$comment_count = mysqli_num_rows($result);

										echo "<div class='huge'>$comment_count</div>";
										
										
										?>

                      <div>Comments</div>
                    </div>
                </div>
            </div>
            <a href="comments.php">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-yellow">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-user fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">

										<?php 
										
										$query = "SELECT * FROM users";
										$result = mysqli_query($connect,$query);
										$user_count = mysqli_num_rows($result);

										echo "<div class='huge'>$user_count</div>";
										
										
										?>
                        <div> Users</div>
                    </div>
                </div>
            </div>
            <a href="users.php">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-red">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-list fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">

										<?php 
										
										$query = "SELECT * FROM categories";
										$result = mysqli_query($connect,$query);
										$category_count = mysqli_num_rows($result);

										echo "<div class='huge'>$category_count</div>";
										
										
										?>
                    <div>Categories</div>
                    </div>
                </div>
            </div>
            <a href="categories.php">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
</div>

<?php 

$query_draft_post = "SELECT * FROM posts WHERE post_status = 'Draft'";
$result_draft_post = mysqli_query($connect,$query_draft_post);
$post_draft_count = mysqli_num_rows($result_draft_post);

$query_published_post = "SELECT * FROM posts WHERE post_status = 'Published'";
$result_published_post = mysqli_query($connect,$query_published_post);
$post_published_count = mysqli_num_rows($result_published_post);

$query_unapproved_comment = "SELECT * FROM comments WHERE comment_status = 'Unapproved'";
$result_unapproved_comment = mysqli_query($connect,$query_unapproved_comment);
$comment_unapproved_count = mysqli_num_rows($result_unapproved_comment);

$query_subscriber = "SELECT * FROM users WHERE user_role = 'Subscriber'";
$result_subscriber = mysqli_query($connect,$query_subscriber);
$subscriber_count = mysqli_num_rows($result_subscriber);
?>
              <!-- /.row -->

							<div class="row">
							
								<script type="text/javascript">google.charts.load('current', {'packages':['bar']});google.charts.setOnLoadCallback(drawChart);

								function drawChart() {
									var data = google.visualization.arrayToDataTable([
										['Date', 'Count'],

										<?php

										$element_text = ['All Posts', 'Draft Posts', 'Published Posts', 'Comments', 'Unapproved Comments', 'Users', 'Subscriber', 'Categories'];
										$element_count = [$post_count, $post_draft_count, $post_published_count, $comment_count, $comment_unapproved_count, $user_count, $subscriber_count, $category_count];

										for($i = 0; $i<8; $i++){

											echo "['{$element_text[$i]}'" . "," . "{$element_count[$i]}],";

										}

										?>

										// ['Posts', 1000],
									]);

									var options = {
										chart: {
											title: '',
											subtitle: '',
										}
									};

									var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

									chart.draw(data, google.charts.Bar.convertOptions(options));
								}
								</script>

								<div id="columnchart_material" style="width: 'auto'; height: 500px;"></div>
							
							</div>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

<?php include "includes/admin_footer.php"; ?>