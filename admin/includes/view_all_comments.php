<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>Id</th>
								<th>Author</th>
								<th>Email</th>
								<th>Content</th>
								<th>Status</th>
								<th>In Response to</th>
								<th>Date</th>
								<th>Approved</th>
								<th>Unapproved</th>
								<th>Delete</th>
							</tr>
						</thead>
						<tbody>

							<?php
							$query = "SELECT * from comments";
							$select_all_comments = mysqli_query($connect, $query);
							while($row = mysqli_fetch_assoc($select_all_comments)){

							$comment_id = $row['comment_id'];
							$comment_post_id = $row['comment_post_id'];
							$comment_author = $row['comment_author'];
							$comment_email = $row['comment_email'];
							$comment_content = $row['comment_content'];
							$comment_status = $row['comment_status'];
							$comment_date = $row['comment_date'];


							$query_post_title = "SELECT * FROM posts WHERE post_id = $comment_post_id";
							$result_post_title = mysqli_query($connect, $query_post_title);
							$row_post_title = mysqli_fetch_assoc($result_post_title);

							echo "<tr>";
							echo "<td>$comment_id</td>";
							echo "<td>$comment_author</td>";



							echo "<td>$comment_email</td>";
							// echo "<td><img src=\"../images/$post_image\" width=\"100px\"></img></td>";
							echo "<td>$comment_content</td>";
							echo "<td>$comment_status</td>";
							echo "<td><a href=\"../post.php?p_id=$comment_post_id\">{$row_post_title['post_title']}</a></td>";         
							echo "<td>$comment_date</td>";         
							echo "<td><a href=\"comments.php?approve=$comment_id\">Approve</td>";                                         
							echo "<td><a href=\"comments.php?unapprove=$comment_id\">Unapprove</td>";                      
							echo "<td><a href=\"comments.php?delete=$comment_id\">Delete</td>";                      
							echo "</tr>";
							}
							?>
						</tbody>
					</table>

					<?php 
					if(isset($_GET['delete'])){
						
						$the_comment_id = $_GET['delete'];
						$query = "DELETE FROM comments WHERE comment_id=$the_comment_id";
						$result = mysqli_query($connect, $query);
						header("Location: comments.php");

						if(!$result){
							die("Query failed". mysqli_error($connect));

						}
					}
					?>

					<?php 
					if(isset($_GET['unapprove'])){
						
						$the_comment_id = $_GET['unapprove'];
						$query_unapprove = "UPDATE comments SET comment_status='UNAPPROVED' WHERE comment_id = $the_comment_id";
						$result_unapprove = mysqli_query($connect, $query_unapprove);
						header("Location: comments.php");

						if(!$result_unapprove){
							die("Query failed". mysqli_error($connect));

						}
					}
					?>

					<?php 
					if(isset($_GET['approve'])){
						
						$the_comment_id = $_GET['approve'];
						$query_approve = "UPDATE comments SET comment_status='APPROVED' WHERE comment_id = $the_comment_id";
						$result_approve = mysqli_query($connect, $query_approve);
						header("Location: comments.php");

						if(!$result_approve){
							die("Query failed". mysqli_error($connect));

						}
					}
					?>