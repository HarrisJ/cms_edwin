<?php 

if(isset($_GET['user_id'])){

	$the_user_id = $_GET['user_id'];

	$query = "SELECT * from users WHERE user_id = $the_user_id";
	$select_users = mysqli_query($connect, $query);
	while($row = mysqli_fetch_assoc($select_users)){

	$user_id = $row['user_id'];
	$username = $row['username'];
	$user_password = $row['user_password'];
	$user_firstname = $row['user_firstname'];
	$user_lastname = $row['user_lastname'];
	$user_email = $row['user_email'];
	$user_role = $row['user_role'];
	$user_image = $row['user_image'];

	}

	if(isset($_POST['edit_user'])){

		$user_firstname = $_POST['user_firstname'];
		$user_lastname = $_POST['user_lastname'];
		$user_role = $_POST['user_role'];

		$user_email = $_POST['user_email'];
		$username = $_POST['username'];
		$user_password = $_POST['user_password'];

		if(!empty($user_password)){

			$query_1 = "SELECT user_password FROM users WHERE user_id = $the_user_id";
			$result_1 = mysqli_query($connect,$query1);
			$row1 = mysqli_fetch_assoc($result1);

			$db_user_password = $row1['user_password'];
			if($db_user_password != $user_password){
				$hashed_password = password_hash($user_password, PASSWORD_BCRYPT, array('cost' => 12) );
			
			}
		
			$query = "UPDATE users SET user_firstname = '$user_firstname', user_lastname = '$user_lastname', user_role = '$user_role', user_email = '$user_email', username = '$username' , user_password = '$hashed_password' WHERE user_id=$the_user_id ";
			$result = mysqli_query($connect, $query);
		
			if(!$result){
				die ("Query Failed". mysqli_error($connect));
			}
		
			echo "User Updated" . "<a href='./users.php'>View Users</a>";

		}

	}

}else{
	header("Location: index.php");
}    
?>

<form action="" method="post" enctype="multipart/form-data">

	<div class="form-group">
		<label for="user_firstname">First Name</label>
		<input type="text" class="form-control" name="user_firstname" value="<?php echo $user_firstname; ?>">
	</div>

	<div class="form-group">
		<label for="user_lastname">Last Name</label>
		<input type="text" class="form-control" name="user_lastname" value="<?php echo $user_lastname; ?>">
	</div>

	<div class="form-group">
		<label for="user_role">User Role</label><br>
		<select name="user_role" class="form-control" id="">
		<option value="<?php echo $user_role; ?>"><?php echo $user_role; ?></option>
		<?php
		if($user_role == 'Admin'){
			echo "<option value=\"Subscriber\">Subscriber</option>";
		}
		else{
			echo "<option value=\"Admin\">Admin</option>";
		}
		?>
		</select>	
	</div>

	<div class="form-group">
		<label for="user_email">Email</label>
		<input type="email" class="form-control" name="user_email" value="<?php echo $user_email; ?>">
	</div>

	<div class="form-group">
		<label for="username">Username</label>
		<input type="text" class="form-control" name="username" value="<?php echo $username; ?>">
	</div>

	<div class="form-group">
		<label for="user_password">Password</label>
		<input type="password" class="form-control" name="user_password" value="" autocomplete="off">
	</div>

	<div class="form-group">
		<input type="submit" class="btn btn-primary" name="edit_user" value="Edit User">
	</div>

</form>





