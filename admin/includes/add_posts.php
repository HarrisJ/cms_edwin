<?php 

if(isset($_POST['create_posts'])){

	$post_title = $_POST['title'];
	$post_category_id = $_POST['post_category_id'];
	$post_author = $_POST['post_author'];
	$post_status = $_POST['post_status'];

	$post_image = $_FILES['post_image']['name'];
	$post_image_temp = $_FILES['post_image']['tmp_name'];
	$post_tags = $_POST['post_tags'];
	$post_content = $_POST['post_content'];
	$post_date = date('d-m-y');
	//$post_comment_count = 0;

	move_uploaded_file($post_image_temp, "../images/$post_image");

	$query = "INSERT INTO posts(post_category_id, post_title, post_author, post_date, post_image, post_content, post_tags, post_status) ";
	$query .= "VALUES ($post_category_id, '$post_title', '$post_author', now(), '$post_image', '$post_content', '$post_tags', '$post_status')";

	$result = mysqli_query($connect, $query);

	if(!$result){
		die ("Query Failed". mysqli_error($connect));
	}

	$the_post_id = mysqli_insert_id($connect);

	echo "<p class='bg-success'>Post Created . <a class='btn btn-success' href='../post.php?p_id=$the_post_id'>View Post Created</a> or <a class='btn btn-primary' href='post.php'>View All Post</a> </p>";

}

?>

<form action="" method="post" enctype="multipart/form-data">

	<div class="form-group">
		<label for="title">Post Title</label>
		<input type="text" class="form-control" name="title">
	</div>


	<div class="form-group">
		<label for="post_category_id">Post Category</label><br>
		<select name="post_category_id" class="form-control" id="">
				<?php 
				$query_dropdown = "SELECT * FROM categories";
				$result_dropdown = mysqli_query($connect,$query_dropdown);
				if(!$result_dropdown){

					die("Query dropdown failed". mysqli_error($connect));
				}
				while($row_dropdown = mysqli_fetch_assoc($result_dropdown)):	?>
					<option value="<?php echo $row_dropdown['cat_id']; ?>"><?php echo $row_dropdown['cat_title']; ?></option>
				<?php endwhile ?>
		</select>
	</div>

	<div class="form-group">
		<label for="author">Author</label>
		<input type="text" class="form-control" name="post_author">
	</div>


	<div class="form-group">
		<label for="post_status">Post Status</label><br>
		<select name="post_status" class="form-control" id="">
		<option value="Published">Published</option>
		<option value="Draft">Draft</option>
		</select>	
	</div>

	<div class="form-group">
		<label for="post_image">Post Image</label>
		<input type="file" class="form-control" name="post_image">
	</div>

	<div class="form-group">
		<label for="post_tags">Post Tag</label>
		<input type="text" class="form-control" name="post_tags">
	</div>

	<div class="form-group">
		<label for="post_content">Post Content</label>
		<textarea class="form-control" name="post_content" id="body" rows="10" cols="30"></textarea>
	</div>

	<div class="form-group">
		<input type="submit" class="btn btn-primary" name="create_posts" value="Publish Post">
	</div>


</form>