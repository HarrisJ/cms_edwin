<?php

	if(isset($_GET['p_id'])){
		$post_id = $post_id_to_display = ($_GET['p_id']);
	}	

	$query_display = "SELECT * FROM posts WHERE post_id=$post_id";
	$result_display = mysqli_query($connect,$query_display);
	$row_display = mysqli_fetch_array($result_display);

	if(!$result_display){

		die("Query display failed". mysqli_error($connect));
	}
?>

<?php 

if(isset($_POST['update_posts'])){

	$post_title = $_POST['title'];
	$post_category_id = $_POST['post_category_id'];
	$post_author = $_POST['post_author'];
	$post_status = $_POST['post_status'];

	$post_image = $_FILES['post_image']['name'];
	$post_image_temp = $_FILES['post_image']['tmp_name'];
	$post_tags = $_POST['post_tags'];
	$post_content = $_POST['post_content'];
	$post_date = date('d-m-y');
	// $post_comment_count = 4;

	move_uploaded_file($post_image_temp, "../images/$post_image");

	// if(empty($post_image)){

	// 	$query_image = "SELECT * FROM posts WHERE post_id = $post_id";
	// 	$result_image = mysqli_query($connect,$query_image);
	// 	while($row_image = mysqli_fetch_assoc($result_image)){

	// 		$post_image = $row_image['post_image'];

	// 	}

//	}

	$query_edit = "UPDATE posts SET post_category_id = $post_category_id, post_title = '$post_title', post_author = '$post_author', post_date = now(), post_image = '$post_image', post_content = '$post_content', post_tags = '$post_tags', post_status = '$post_status' WHERE post_id = $post_id";
	$result_edit = mysqli_query($connect, $query_edit);
	if(!$result_edit){

		die("Query edit failed". mysqli_error($connect));
	}

	echo "<p class='bg-success'>Post Update . <a class='btn btn-primary' href='../post.php?p_id=$post_id'>View Post</a> or <a class='btn btn-primary' href='post.php'>Edit More Posts</a></p>";

	// echo "<a href=\"post.php?source=edit_posts&p_id=$post_id\">";                                         

}

?>

<form action="" method="post" enctype="multipart/form-data">

	<div class="form-group">
		<label for="title">Post Title</label>
		<input type="text" class="form-control" name="title" value="<?php echo $row_display['post_title'] ?>">
	</div>

	<div class="form-group">
		<select name="post_category_id" id="" class="form-control">
				<?php $query_dropdown = "SELECT * FROM categories";
				$result_dropdown = mysqli_query($connect,$query_dropdown);
				if(!$result_dropdown){

					die("Query dropdown failed". mysqli_error($connect));
				}
				while($row_dropdown = mysqli_fetch_assoc($result_dropdown)):	?>
					<option value="<?php echo $row_dropdown['cat_id']; ?>"><?php echo $row_dropdown['cat_title']; ?></option>
				<?php endwhile ?>
		</select>
	</div>

	<div class="form-group">
		<label for="author">Author</label>
		<input type="text" class="form-control" name="post_author" value="<?php echo $row_display['post_author'] ?>">
	</div>

	<div class="form-group">
		<label for="post_status">Post Status</label>
		<select name="post_status" id="" class="form-control">
		<option value="Published" <?php echo ($row_display['post_status']== 'Published') ?'selected':'' ?> >Published</option>
		<option value="Draft" <?php echo ($row_display['post_status']== 'Draft') ?'selected':'' ?> >Draft</option>		
		</select>
	</div>

	<div class="form-group">
		<label for="post_image">Post Image</label>
		<input type="file" class="form-control" name="post_image">
		<img src="../images/<?php echo $row_display['post_image'] ?>" alt="" height="100px" height="100px">
		
	</div>

	<div class="form-group">
		<label for="post_tags">Post Tag</label>
		<input type="text" class="form-control" name="post_tags" value="<?php echo $row_display['post_tags'] ?>">
	</div>

	<div class="form-group">
		<label for="post_content">Post Content</label>
		<textarea class="form-control" name="post_content" cols="30" rows="10"><?php echo $row_display['post_content'] ?></textarea>
	</div>

	<div class="form-group">
		<input type="submit" class="btn btn-primary" name="update_posts" value="Publish Post">
	</div>


</form>