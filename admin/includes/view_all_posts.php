<?php 

if(isset($_POST['checkBoxArray'])){

	foreach($_POST['checkBoxArray'] as $postvalueid){

		$bulk_options = $_POST['bulk_options'];

		switch($bulk_options){

			case 'Published':
			$query_publish_only = "UPDATE posts set post_status = '$bulk_options' WHERE post_id = $postvalueid";
			$update_to_publish = mysqli_query($connect, $query_publish_only);
			break;

			case 'Draft':
			$query_draft_only = "UPDATE posts set post_status = '$bulk_options' WHERE post_id = $postvalueid";
			$update_to_draft = mysqli_query($connect, $query_draft_only);
			break;

			case 'delete':
			$query = "DELETE FROM posts WHERE post_id=$postvalueid";
			$result = mysqli_query($connect, $query);
			break;
			
			case 'clone':
			$query = "SELECT * FROM posts WHERE post_id=$postvalueid";
			$select_post_query = mysqli_query($connect, $query);
			if(!$select_post_query){
				die ("Failed" . mysqli_error($connect));
			}

			while($row = mysqli_fetch_assoc($select_post_query)){

				$post_title = $row['post_title'];
				$post_category_id = $row['post_category_id'];
				$post_date = $row['post_date'];
				$post_author = $row['post_author'];
				$post_status = $row['post_status'];
				$post_image = $row['post_image'];
				$post_tags = $row['post_tags'];
				$post_content = $row['post_content'];
			}

			$query = "INSERT INTO posts(post_category_id, post_title, post_author, post_date, post_image, post_content, post_tags, post_status) ";
			$query .= "VALUES ($post_category_id, '$post_title', '$post_author', now(), '$post_image', '$post_content', '$post_tags', '$post_status')";
			$result_copy = mysqli_query($connect, $query);
			break;
			
			default;


		}
	}

}

?>

<form action="" method="post">

						<table class="table table-bordered table-hover">

							<div id="bulkOptionContainer" class="col-xs-4" style="padding: 0px;">
										<select name="bulk_options" id="" class="form-control">
											<option value="">Select Option</option>
											<option value="Published">Publish</option>
											<option value="Draft">Draft</option>
											<option value="delete">Delete</option>
											<option value="clone">Clone</option>
										</select>
							</div>

							<div class="col-xs-4">
								<input type="submit" name="submit" class="btn btn-success" value="Apply">
								<a class="btn btn-primary" href="post.php?source=add_posts">Add New</a>
							</div>

						<thead>
							<tr>
								<th><input id="selectAllBoxes" type="checkbox"></th>
								<th>Id</th>
								<th>Author</th>
								<th>Title</th>
								<th>Category</th>
								<th>Status</th>
								<th>Image</th>
								<th>Tags</th>
								<th>Comments</th>
								<th>Date</th>
								<th>Action</th>
								<th>Action</th>
								<th>Action</th>
								<th>Post View Count</th>
							</tr>
						</thead>
						<tbody>

							<?php
							$query = "SELECT * from posts ORDER BY post_id DESC";
							$select_all_posts = mysqli_query($connect, $query);
							while($row = mysqli_fetch_assoc($select_all_posts)){

							$post_id = $row['post_id'];
							$post_author = $row['post_author'];
							$post_title = $row['post_title'];
							$post_category = $row['post_category_id'];
							$post_status = $row['post_status'];
							$post_image = $row['post_image'];
							$post_tags = $row['post_tags'];
							$post_comments = $row['post_comment_count'];
							$post_date = $row['post_date'];
							$post_view_count = $row['post_view_count'];

							echo "<tr>";

							echo "<td>$post_id</td>";
							echo "<td>$post_author</td>";
							echo "<td>$post_title</td>";

							$query = "SELECT * FROM categories WHERE cat_id = $post_category";
							$select_categories_id = mysqli_query($connect, $query);
							if(!$select_categories_id){
									die("UPDATE failed" . mysqli_error($connect));
							}
							$row = mysqli_fetch_assoc($select_categories_id);
							echo "<td>{$row['cat_title']}</td>";

							echo "<td>$post_status</td>";
							echo "<td><img src=\"../images/$post_image\" width=\"100px\"></img></td>";
							echo "<td>$post_tags</td>";

							$query_count = "SELECT * FROM comments WHERE comment_post_id = $post_id";
							$result_count = mysqli_query($connect,$query_count);
							$row_comment_id = mysqli_fetch_assoc($result_count);
							$comment_id = $row_comment_id['comment_id'];
							$count_comment = mysqli_num_rows($result_count);

							
							echo "<td><a href=\"post_comment.php?id=$post_id\">$count_comment</td>";
							echo "<td>$post_date</td>";         
							echo "<td><a href=\"../post.php?p_id=$post_id\" class=\"btn btn-success \">View Post</td>";                                         
							echo "<td><a href=\"post.php?source=edit_posts&p_id=$post_id\">Edit</td>";                                         
							echo "<td><a onClick=\"javascript: return confirm('Are you sure want to delete?')\" href=\"post.php?delete=$post_id\">Delete</td>";  
							echo "<td><a href=\"post.php?reset=$post_id\"</a>$post_view_count</td>";               
							echo "</tr>";
							}
							?>
						</tbody>
					</table>
</form>

					<?php 
					if(isset($_GET['delete'])){
						
						$the_post_id = $_GET['delete'];
						$query = "DELETE FROM posts WHERE post_id=$the_post_id";
						$result = mysqli_query($connect, $query);
						header("Location: post.php");


						if(!$result){
							die("Query failed". mysqli_error($connect));

						}
					}

					if(isset($_GET['reset'])){
						
						$the_post_id = $_GET['reset'];
						$query = "UPDATE posts SET post_view_count = 0 WHERE post_id=$the_post_id";
						$result = mysqli_query($connect, $query);
						header("Location: post.php");


						if(!$result){
							die("Query failed". mysqli_error($connect));

						}
					}
					?>