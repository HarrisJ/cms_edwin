<?php

include "includes/db.php";
include "includes/header.php";
    
?>

<body>

    <!-- Navigation -->
    <?php include "includes/navigation.php";?>
   

    <!-- Page Content -->
    <div class="container">

        <div class="row">
            <!-- Blog Entries Column -->
            <div class="col-md-8">
                
              <?php

							if(isset($_GET['p_id'])){
								$the_post_id = $_GET['p_id'];

								$view_query = "UPDATE posts SET post_view_count = post_view_count + 1 WHERE post_id = $the_post_id";
								$send_query = mysqli_query($connect, $view_query);

								if(!$send_query){
									die("query failed");
								}
							

                $query = "SELECT * FROM posts WHERE post_id = $the_post_id";
                $select_all_post_query = mysqli_query($connect,$query);
                while($row = mysqli_fetch_assoc($select_all_post_query)){
                    $post_id = $row['post_id'];
                    $post_title = $row['post_title'];
                    $post_author = $row['post_author'];
                    $post_date = $row['post_date'];
                    $post_image = $row['post_image'];
                    $post_content = $row['post_content'];
                    
                ?>
                <h1 class="page-header">
                    Page Heading
                    <small>Secondary Text</small>
                </h1>

                <!-- First Blog Post -->
                <h2>
                    <a href="#"><?php echo $post_title; ?></a>
                </h2>
                <p class="lead">
                    by <a href="index.php"><?php echo $post_author; ?></a>
                </p>
                <p><span class="glyphicon glyphicon-time"></span> Posted on <?php echo $post_date; ?></p>
                <hr>
                <img class="img-responsive" src="images/<?php echo $post_image; ?>" alt="">
                <hr>
                <p><?php echo $post_content; ?></p>                
                <?php    
								}
								
							}else{

								header("Location: index.php");
							}
                ?>


                <!-- Blog Comments -->

								<?php 
								
								if(isset($_POST['create_comment'])){

									$the_post_id = $_GET['p_id'];
									$comment_author = $_POST['comment_author'];
									$comment_email = $_POST['comment_email'];
									$comment_content = $_POST['comment'];

									if(!empty($comment_author) && !empty($comment_email) && !empty($comment_content)){

										$query = "INSERT INTO comments(comment_post_id, comment_author, comment_email, comment_content, comment_status, comment_date) VALUES ($the_post_id, '$comment_author', '$comment_email', '$comment_content', 'UNAPPROVED', now())";

										$result = mysqli_query($connect, $query);
	
										// $query2 = "UPDATE posts SET post_comment_count = post_comment_count + 1 ";
										// $query2 .= "WHERE post_id = $the_post_id";	
										// $result2 = mysqli_query($connect, $query2);
										// if(!$result2){
										// 	die("Query failed" . mysqli_error($connect));
										// }

									}else{
										echo "<script>alert('Field Could not be empty')</script>";
									}



								}
								?>

                <!-- Comments Form -->
                <div class="well">
                    <h4>Leave a Comment:</h4>
                    <form role="form" action="" method="post">

												<div class="form-group">
													<label for="comment_author">Author</label>
													<input type="text" class="form-control" name="comment_author">
												</div>

												<div class="form-group">
													<label for="comment_email">Email</label>
													<input type="email" class="form-control" name="comment_email">
												</div>

                        <div class="form-group">
														<label for="comment">Your Comment</label>
                            <textarea class="form-control" rows="3" name="comment"></textarea>
                        </div>

                        <button type="submit" class="btn btn-primary" name="create_comment">Submit</button>
                    </form>
                </div>

                <hr>

                <!-- Posted Comments -->
								
								<?php 
								
								$query_comments = "SELECT * from comments WHERE comment_post_id = $the_post_id ";
								$query_comments .= "AND comment_status = \"APPROVED\" ";
								$query_comments .= "ORDER BY comment_id DESC";
								$select_comments_to_display = mysqli_query($connect, $query_comments);
								while($row_comments = mysqli_fetch_assoc($select_comments_to_display)){

									$comment_author = $row_comments['comment_author'];
									$comment_date = $row_comments['comment_date'];
									$comment_content = $row_comments['comment_content'];

								?>

									<div class="media">
									<a class="pull-left" href="#">
											<img class="media-object" src="http://placehold.it/64x64" alt="">
									</a>
									<div class="media-body">
											<h4 class="media-heading"><?php echo $comment_author ?>
													<small><?php echo $comment_date ?></small>
											</h4>
											<?php echo $comment_content ?>
									</div>
									</div>

								<?php
								}
								?>
                <!-- Comment -->
            </div>

            <!-- Blog Sidebar Widgets Column -->
            <?php include "includes/sidebar.php";?>
            
            
        

        </div>
        <!-- /.row -->
        
        <!-- /.footer -->
        <?php
        include "includes/footer.php";
        
        ?>

        <hr>

       